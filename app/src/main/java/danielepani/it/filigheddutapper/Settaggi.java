package danielepani.it.filigheddutapper;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Settings fragment. Just basic things
 *
 */

public class Settaggi extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {

    /**
     * This one acts like onCreate for activities
     * @param savedInstanceState
     * @param rootKey
     * @link https://developer.android.com/reference/android/support/v7/preference/PreferenceFragmentCompat
     */
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs);

        PreferenceScreen preferenceScreen = getPreferenceScreen();
        SharedPreferences sharedPreferences = preferenceScreen.getSharedPreferences();

        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        //int count = preferenceScreen.get
        int count = preferenceScreen.getPreferenceCount();

        for (int i=0; i<count; i++) {
            Preference p = preferenceScreen.getPreference(i);

            if (p instanceof EditTextPreference) {
                String v = sharedPreferences.getString(p.getKey(), "");
                p.setSummary(v);
            }
        }

    }

    /**
     * This decides what is done when a preference is updated. Needed to implement onSharedPreferenceChangeListener
     * @param p changed preference
     * @param newValue new preference value
     * @return bool
     */

    @Override
    public boolean onPreferenceChange(Preference p, Object newValue) {

        return false;
    }

    /**
     * This decides what is done when a sharedpreference is updated. Needed to implement onSharedPreferenceChangeListener. This is actually the executed one
     * @param sharedPreferences the SharedPreferences instance
     * @param key preference key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference p = findPreference(key);
        if (null != p) {
            if (p instanceof EditTextPreference) {
                String v = sharedPreferences.getString(p.getKey(), "");
                p.setSummary(v);
            }
        }
    }
}
