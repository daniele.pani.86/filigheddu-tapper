package danielepani.it.filigheddutapper.scoresdata;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Db helper for local db. This is necessary to store scores data.
 */

public class ScoresDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "scores.db";
    private static final int VERSION = 2;

    public ScoresDbHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    /**
     * Actions to be taken when db is created
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "CREATE TABLE " + ScoresContract.TABLE_NAME + " ( " +
                ScoresContract.ScoresEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ScoresContract.ScoresEntry.PLAYER_NAME + " TEXT NOT NULL, " +
                ScoresContract.ScoresEntry.PLAYER_SCORE + " INTEGER NOT NULL, " +
                ScoresContract.ScoresEntry.PLAYER_GAME_DATE + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(SQL_CREATE_TABLE);
    }


    /**
     * Actions to be taken when db version is upgraded
     * @param db
     * @param oldVersion existing db version
     * @param newVersion new db version
     *
     * TODO: make it so db is not entirely deleted in case of upgrade
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE if EXISTS " + ScoresContract.TABLE_NAME);
        onCreate(db);
    }
}
