package danielepani.it.filigheddutapper.scoresdata;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Contract class. It defines the db structure and more constants for ContentProvider
 *
 */

public class ScoresContract {
    public static final String TABLE_NAME = "scores";

    //For ScoresContentProvider
    public static final String AUTHORITY = "danielepani.it.filigheddutapper";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_SCORES = "overallscores";

    public static class ScoresEntry implements BaseColumns {
        //For ScoresContentProvider
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SCORES).build();

        //Db columns
        public static final String PLAYER_NAME = "name";
        public static final String PLAYER_SCORE = "score";
        public static final String PLAYER_GAME_DATE = "gameDate";
    }
}
