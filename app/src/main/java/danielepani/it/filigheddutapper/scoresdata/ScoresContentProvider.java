package danielepani.it.filigheddutapper.scoresdata;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Although unnecessary, this is a ContentProvider for scores data
 *
 */

public class ScoresContentProvider extends ContentProvider {

    public static final int SCORES = 100;
    private static final UriMatcher uriMatcher = returnUriMatcher();

    private ScoresDbHelper dbHelper;


    /**
     * Create the UriMatcher to use
     * @return the UriMatcher
     */
    public static UriMatcher returnUriMatcher () {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(ScoresContract.AUTHORITY, ScoresContract.PATH_SCORES, SCORES);

        return uriMatcher;
    }

    /**
     * This acts like a constructor: it assigns instance vars and creates the ContentProvider Object
     * @return boolean
     * @link https://developer.android.com/guide/topics/providers/content-providers
     */

    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new ScoresDbHelper(context);
        return true;
    }

    /**All of the following are required by ContentProvider interface; not all of them will be used by this app. */
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    /**
     * Create a new Score entry
     * @param uri the action URI
     * @param values ContentValues with all data to insert in our local SQLite storage
     * @return Uri the Uri for new data
     */
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int match = uriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case (SCORES) :
                long r = db.insert(ScoresContract.TABLE_NAME, null, values);

                if (r > 0) {
                    returnUri = ContentUris.withAppendedId(ScoresContract.ScoresEntry.CONTENT_URI, r);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;

            default:
                throw new UnsupportedOperationException("What the fkk???");
        }
        //Mandatory after insert
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    /**
     * Get all the data and create a Cursor with them
     * @param uri the action Uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder i'm setting it inside the function
     * @return the cursor containing all the data
     */
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        int match = uriMatcher.match(uri);

        switch (match) {
            case SCORES:
                return db.query(ScoresContract.TABLE_NAME,
                        null,
                        null,
                        null,
                        null,
                        null,
                        ScoresContract.ScoresEntry.PLAYER_SCORE + " DESC"
                );

            default:
                throw new UnsupportedOperationException("What the...");
        }
    }
}
