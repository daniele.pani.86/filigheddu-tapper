package danielepani.it.filigheddutapper;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;

import org.w3c.dom.Text;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import danielepani.it.filigheddutapper.match.Match;
import danielepani.it.filigheddutapper.scoresdata.ScoresContract;
import danielepani.it.filigheddutapper.utils.Notifications;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * This is the gaming screen. Nothing big, just a common android activity with keys and updates
 * TODO: replace all string concatenations with Integer.parseString(int n) calls
 */
public class SchermataDiGioco extends AppCompatActivity  {

    //These static instance vars are necessary to determine if a match is paused or there's none
    public static SchermataDiGioco currentInstance;
    public static boolean isRunning = false;

    //The following vars are the Activity's views
    private ImageView enemyFaceImageView;

    private Button startGameButton;

    private TextView hpAmountTextViewLabel;
    private TextView moneyAmountTextViewLabel;
    private TextView minionsAmountTextViewLabel;
    private TextView minersAmountTextViewLabel;
    private TextView swordPowerTextViewLabel;
    private TextView playerHpAmountTextViewLabel;
    private TextView gameEndTextViewLabel;

    private TextView hpAmountTextView;
    private TextView moneyAmountTextView;
    private TextView minionsAmountTextView;
    private TextView minersAmountTextView;
    private TextView swordPowerTextView;
    private TextView playerHpAmountTextView;
    private TextView gameEndTextView;

    private Button attackButton;
    private Button powerSwordUpButton;
    private Button getOneMinionButton;
    private Button powerMinionsUpButton;
    private Button workHardButton;
    private Button improveEarningsButton;
    private Button getOneMinerButton;
    private Button powerMinersUpButton;

    private Button restartGameButton;

    private EditText playerName;
    private TextView playerNameLabel;

    //This is the match object. @see match/Match
    Match match;

    //Timer variables
    Timer minionsTimer;
    Timer enemyAttackTimer;

    //These booleans determine if score has been updated or saved in the local db
    boolean alreadySaved = false;
    boolean alreadyUpdated = false;


    /**
     * onCreate is like a constructor: it assigns all instance variables and handles the instanceState, if any
     *
     * @param savedInstanceState the bundle containing all match data, if activity was put in background
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        currentInstance = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_di_gioco);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        playerName = findViewById(R.id.player_name);
        playerNameLabel = findViewById(R.id.player_name_label);

        startGameButton = findViewById(R.id.start_game_button);

        enemyFaceImageView = findViewById(R.id.enemy_face_image_view);

        hpAmountTextView  = findViewById(R.id.hp_amount);
        hpAmountTextViewLabel  = findViewById(R.id.hp_amount_label);

        moneyAmountTextView = findViewById(R.id.money_amount);
        moneyAmountTextViewLabel = findViewById(R.id.money_amount_label);

        minionsAmountTextView = findViewById(R.id.minions_amount);
        minionsAmountTextViewLabel = findViewById(R.id.minions_amount_label);

        minersAmountTextView = findViewById(R.id.miners_amount);
        minersAmountTextViewLabel = findViewById(R.id.miners_amount_label);

        swordPowerTextView = findViewById(R.id.sword_power);
        swordPowerTextViewLabel = findViewById(R.id.sword_power_label);

        playerHpAmountTextView = findViewById(R.id.player_hp_amount);
        playerHpAmountTextViewLabel = findViewById(R.id.player_hp_amount_label);

        attackButton = findViewById(R.id.attack_button);
        powerSwordUpButton = findViewById(R.id.power_sword_up_button);
        getOneMinionButton = findViewById(R.id.get_one_minion_button);
        powerMinionsUpButton = findViewById(R.id.power_minions_up_button);
        workHardButton = findViewById(R.id.work_hard_button);
        improveEarningsButton = findViewById(R.id.improve_earnings_button);
        getOneMinerButton = findViewById(R.id.get_one_miner_button);
        powerMinersUpButton = findViewById(R.id.power_miners_up_button);

        gameEndTextView = findViewById(R.id.game_end_text_view);

        restartGameButton = findViewById(R.id.restart_game_button);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("enemyRemainingHp")) {
                match = null;
                Log.w("onCreate", "SavedInstance is not null");
                match = new Match(savedInstanceState.getString("playerName"), savedInstanceState.getInt("enemyInitialHp"), savedInstanceState.getInt("playerInitialHp"));

                alreadySaved = savedInstanceState.getBoolean("alreadySaved");
                alreadyUpdated = savedInstanceState.getBoolean("alreadyUpdated");

                match.setEnemyRemainingHp(savedInstanceState.getInt("enemyRemainingHp"));
                match.setEnemyFaceDrawableId(savedInstanceState.getInt("enemyFaceDrawableId"));
                match.setEnemyAttackPower(savedInstanceState.getInt("enemyInitialHp")*savedInstanceState.getInt("step")/100);

                match.setPlayerRemainingHp(savedInstanceState.getInt("playerRemainingHp"));
                match.setPlayerSwordPower(savedInstanceState.getInt("playerSwordPower"));
                match.setMinionsPower(savedInstanceState.getInt("minionsPower"));
                match.setMinionsNumber(savedInstanceState.getInt("minionsNumber"));
                match.setMinersPower(savedInstanceState.getInt("minersPower"));
                match.setMinersNumber(savedInstanceState.getInt("minersNumber"));
                match.setMoney(savedInstanceState.getInt("money"));
                match.setMoneyPerTick(savedInstanceState.getInt("moneyPerTick"));
                match.setStep(savedInstanceState.getInt("step"));
                match.setScore(savedInstanceState.getInt("score"));

                gameRestart();
                if (enemyAttackTimer == null && minionsTimer == null)
                timerCreator();
            }
        }
    }

    /**
     * Pause state
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     * I chose to display a notification if there's a paused match; i delete all timers as well
     */

    @Override
    protected void onPause() {
        super.onPause();
        if (match != null) {
            Notifications.imStillThere(this);
            enemyAttackTimer.cancel();
            minionsTimer.cancel();
            enemyAttackTimer = null;
            minionsTimer = null;
        }
    }

    /**
     * Activity was resumed
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     * Previous views are restored, if a match was started; timers are recreated and restarted
     */
    @Override
    protected void onResume() {
        super.onResume();
        Notifications.iSawYouGaveMeAttention(getApplicationContext());
        if (match != null) {

            playerName.setVisibility(View.GONE);
            playerNameLabel.setVisibility(View.GONE);
            startGameButton.setVisibility(View.GONE);

            hpAmountTextView.setVisibility(View.VISIBLE);
            playerHpAmountTextView.setVisibility(View.VISIBLE);
            moneyAmountTextView.setVisibility(View.VISIBLE);
            minionsAmountTextView.setVisibility(View.VISIBLE);
            minersAmountTextView.setVisibility(View.VISIBLE);
            enemyFaceImageView.setVisibility(View.VISIBLE);
            swordPowerTextView.setVisibility(View.VISIBLE);
            swordPowerTextViewLabel.setVisibility(View.VISIBLE);
            hpAmountTextViewLabel.setVisibility(View.VISIBLE);
            playerHpAmountTextViewLabel.setVisibility(View.VISIBLE);
            moneyAmountTextViewLabel.setVisibility(View.VISIBLE);
            minionsAmountTextViewLabel.setVisibility(View.VISIBLE);
            minersAmountTextViewLabel.setVisibility(View.VISIBLE);

            attackButton.setVisibility(View.VISIBLE);
            powerSwordUpButton.setVisibility(View.VISIBLE);
            getOneMinionButton.setVisibility(View.VISIBLE);
            powerMinionsUpButton.setVisibility(View.VISIBLE);
            workHardButton.setVisibility(View.VISIBLE);
            improveEarningsButton.setVisibility(View.VISIBLE);
            getOneMinerButton.setVisibility(View.VISIBLE);
            powerMinersUpButton.setVisibility(View.VISIBLE);

            hpAmountTextView.setText("" + match.getEnemyRemainingHp());
            playerHpAmountTextView.setText("" + match.getPlayerRemainingHp());
            moneyAmountTextView.setText("" + match.getMoney());
            minionsAmountTextView.setText("" + match.getMinionsNumber());
            swordPowerTextView.setText("" + match.getPlayerSwordPower());
            minersAmountTextView.setText("" + match.getMinersNumber());
            enemyFaceImageView.setImageResource(match.getEnemyFaceDrawableId());

            timerCreator();
        }
    }

    /**
     * Activity is started
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     * isRunning is set to true, so any activity knows this one is in background if needed
     */

    @Override
    protected void onStart() {
        super.onStart();
        isRunning = true;
    }

    /**
     * Activity is destroyed
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     * isRunning is set to false, so any activity knows this one is not running if needed
     */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
        Notifications.iSawYouGaveMeAttention(this);
    }

    /**
     * Save instance state, especially useful when device has been rotated
     *
     * @param outState the bundle that will be saved
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     * Just saving all Match data; i'll restore them in onCreate
     */

    @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            Log.w("onSaveInstanceState", "Bundle is going to be created");

            if (match != null) {
                outState.putBoolean("alreadySaved", alreadySaved);
                outState.putBoolean("alreadyUpdated", alreadyUpdated);

                outState.putInt("enemyInitialHp", match.getEnemyInitialHp());
                outState.putInt("enemyAttackPower", match.getEnemyAttackPower());
                outState.putInt("enemyRemainingHp", match.getEnemyRemainingHp());
                outState.putInt("enemyFaceDrawableId", match.getEnemyFaceDrawableId());

                outState.putString("playerName", match.getPlayerName());
                outState.putInt("playerInitialHp", match.getPlayerInitialHp());
                outState.putInt("playerRemainingHp", match.getPlayerRemainingHp());
                outState.putInt("playerSwordPower", match.getPlayerSwordPower());
                outState.putInt("minionsPower", match.getMinionsPower());
                outState.putInt("minionsNumber", match.getMinionsNumber());
                outState.putInt("minersPower", match.getMinersPower());
                outState.putInt("minersNumber", match.getMinersNumber());
                outState.putInt("money", match.getMoney());
                outState.putInt("moneyPerTick", match.getMoneyPerTick());
                outState.putInt("step", match.getStep());
                outState.putInt("score", match.getScore());
            }
        }

    /**
     * Handle the back arrow, going back to main screen
     *
     * @param item the MenuItem; in this case, it's only the back button
     * @return bool true if menu item has been pressed
     * @link https://developer.android.com/guide/topics/ui/menus
     *
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent homeIntent = new Intent(this, SchermataPrincipale.class);
            startActivity(homeIntent);
            return true;
        }
        return false;
    }

    /**
     * Just adjusts all the views when you or the enemy die (in game of course); it saves score as well
     */
    private void gameOver () {
        attackButton.setVisibility(View.GONE);
        powerSwordUpButton.setVisibility(View.GONE);
        getOneMinionButton.setVisibility(View.GONE);
        powerMinionsUpButton.setVisibility(View.GONE);
        workHardButton.setVisibility(View.GONE);
        improveEarningsButton.setVisibility(View.GONE);
        getOneMinerButton.setVisibility(View.GONE);
        powerMinersUpButton.setVisibility(View.GONE);

        gameEndTextView.setVisibility(View.VISIBLE);
        restartGameButton.setVisibility(View.VISIBLE);
        if (!match.isPlayerAlive()) {
            saveScore();
        }
    }

    /**
     *
     * After gameOver(), you may choose to continue or restart
     *
     */
    private void gameRestart () {
        attackButton.setVisibility(View.VISIBLE);
        powerSwordUpButton.setVisibility(View.VISIBLE);
        getOneMinionButton.setVisibility(View.VISIBLE);
        powerMinionsUpButton.setVisibility(View.VISIBLE);
        workHardButton.setVisibility(View.VISIBLE);
        improveEarningsButton.setVisibility(View.VISIBLE);
        getOneMinerButton.setVisibility(View.VISIBLE);
        powerMinersUpButton.setVisibility(View.VISIBLE);

        gameEndTextView.setVisibility(View.GONE);
        restartGameButton.setVisibility(View.GONE);

        hpAmountTextView.setText("" + match.getEnemyRemainingHp());
        playerHpAmountTextView.setText("" + match.getPlayerRemainingHp());
        moneyAmountTextView.setText("" + match.getMoney());
        minionsAmountTextView.setText("" + match.getMinionsNumber());
        swordPowerTextView.setText("" + match.getPlayerSwordPower());
        minersAmountTextView.setText("" + match.getMinersNumber());
        enemyFaceImageView.setImageResource(match.getEnemyFaceDrawableId());
        timerCreator();
    }

    /**
     * Attack the enemy
     * @param view the attack button
     */
    public void attack (View view) {
        match.playerAttack();
        hpAmountTextView.setText(""+match.getEnemyRemainingHp());
        enemyFaceImageView.setImageResource(match.getEnemyFaceDrawableId());
        if (!match.isEnemyAlive()) {
            gameOver();
            gameEndTextView.setText(getString(R.string.you_won));
            restartGameButton.setText(getString(R.string.next_level));
        }
    }

    /**
     * Power up the sword
     * @param view the powerSwordUp button
     */
    public void powerSwordUp (View view) {
        match.powerSwordUp(this);
        swordPowerTextView.setText(match.getPlayerSwordPower() + "");
        moneyAmountTextView.setText(match.getMoney()+"");
    }

    /**
     * Get one minion and pay it
     * @param view the getOneMinion button
     */
    public void getOneMinion (View view) {
        match.getOneMinion(this);
        minionsAmountTextView.setText("" + match.getMinionsNumber());
        moneyAmountTextView.setText(match.getMoney()+"");
    }

    /**
     * Power up your minions
     * @param view the powerMinionsUp button
     */
    public void powerMinionsUp (View view) {
        match.increaseMinionPower(this);
        moneyAmountTextView.setText(match.getMoney()+"");
    }

    /**
     * Work for money
     * @param view the workHard button
     */
    public void workHard (View view) {
        match.workHard();
        moneyAmountTextView.setText(match.getMoney() + "");
    }

    /**
     * Get one miner and pay it
     * @param view the getOneMinion button
     */
    public void getOneMiner (View view) {
        match.getOneMiner(this);
        minersAmountTextView.setText("" + match.getMinersNumber());
        moneyAmountTextView.setText(match.getMoney()+"");
    }

    /**
     * Power up your miners
     * @param view the powerMinionsUp button
     */
    public void powerMinersUp (View view) {
        match.increaseMinersPower(this);
        moneyAmountTextView.setText(match.getMoney()+"");
    }

    /**
     * Improve your earnings while working
     * @param view the improveEarnings button
     */
    public void improveEarnings (View view) {
        match.improveEarnings(this);
        moneyAmountTextView.setText(match.getMoney()+"");
    }

    /**
     * If you're in game already, restart it
     * @param view the restartGame button
     */
    public void restartGame (View view) {
        match.reinitializePlayer();
        match.nextStep();
        gameRestart();
        alreadySaved = false;
        alreadyUpdated = false;
    }

    /**
     * Start the game the first time
     * @param view the StartGame button
     */
    public void startGame(View view) {
        alreadySaved = false;
        alreadyUpdated = false;
        String playerNameText = playerName.getText().toString();

        playerName.setVisibility(View.GONE);
        playerNameLabel.setVisibility(View.GONE);
        startGameButton.setVisibility(View.GONE);

        hpAmountTextView.setVisibility(View.VISIBLE);
        playerHpAmountTextView.setVisibility(View.VISIBLE);
        moneyAmountTextView.setVisibility(View.VISIBLE);
        minionsAmountTextView.setVisibility(View.VISIBLE);
        minersAmountTextView.setVisibility(View.VISIBLE);
        enemyFaceImageView.setVisibility(View.VISIBLE);
        swordPowerTextView.setVisibility(View.VISIBLE);
        swordPowerTextViewLabel.setVisibility(View.VISIBLE);
        hpAmountTextViewLabel.setVisibility(View.VISIBLE);
        playerHpAmountTextViewLabel.setVisibility(View.VISIBLE);
        moneyAmountTextViewLabel.setVisibility(View.VISIBLE);
        minionsAmountTextViewLabel.setVisibility(View.VISIBLE);
        minersAmountTextViewLabel.setVisibility(View.VISIBLE);

        attackButton.setVisibility(View.VISIBLE);
        powerSwordUpButton.setVisibility(View.VISIBLE);
        getOneMinionButton.setVisibility(View.VISIBLE);
        powerMinionsUpButton.setVisibility(View.VISIBLE);
        workHardButton.setVisibility(View.VISIBLE);
        improveEarningsButton.setVisibility(View.VISIBLE);
        getOneMinerButton.setVisibility(View.VISIBLE);
        powerMinersUpButton.setVisibility(View.VISIBLE);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int enemyHp;
        int playerHp;

        /* In case value is not in Integer's limits, the vars will assume the max value possible */
        try {
            enemyHp = Integer.parseInt(sharedPreferences.getString("enemy_hp", ""));
        } catch (NumberFormatException e) {
            enemyHp = Integer.MAX_VALUE;
        }

        try {
            playerHp = Integer.parseInt(sharedPreferences.getString("player_hp", ""));
        } catch (NumberFormatException e) {
            playerHp = Integer.MAX_VALUE;
        }

        match = new Match(playerNameText, enemyHp, playerHp);

        hpAmountTextView.setText("" + match.getEnemyRemainingHp());
        playerHpAmountTextView.setText("" + match.getPlayerRemainingHp());
        moneyAmountTextView.setText("" + match.getMoney());
        minionsAmountTextView.setText("" + match.getMinionsNumber());
        swordPowerTextView.setText("" + match.getPlayerSwordPower());
        minersAmountTextView.setText("" + match.getMinersNumber());
        enemyFaceImageView.setImageResource(match.getEnemyFaceDrawableId());

        timerCreator();
    }

    /**
     * While playing, update match's score everytime enemy or player die
     * @param event Win or loss
     * TODO: create constant String values for event
     */
    private void updateScore (String event) {
        if (!alreadyUpdated) {
            switch (event) {
                case "Victory":
                    match.setScore(match.getScore() + match.getEnemyInitialHp()/match.getPlayerInitialHp()*match.getStep()+match.getPlayerRemainingHp());
                    break;

                case "Loss":
                    match.setScore(match.getScore() + (match.getEnemyInitialHp()*match.getStep() - match.getEnemyRemainingHp())/match.getPlayerInitialHp());
                    break;

                default:
                    match.setScore(match.getScore());
                    break;
            }
            alreadyUpdated = true;
            Log.w("updateScore", match.getScore()+"");
        }
    }

    /**
     * Core of the game. Timers will execute enemy and minion attacks
     */
    private void timerCreator () {
        if (enemyAttackTimer != null) {
            enemyAttackTimer.cancel();
            enemyAttackTimer = null;
        }
        if (minionsTimer != null) {
            minionsTimer.cancel();
            minionsTimer = null;
        }
        enemyAttackTimer = new Timer();
        enemyAttackTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (match.isPlayerAlive()) {
                    match.enemyAttack();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        playerHpAmountTextView.setText(match.getPlayerRemainingHp() + "");
                        if (!match.isPlayerAlive()) {
                            updateScore("Loss");
                            gameOver();
                            gameEndTextView.setText(getString(R.string.youre_dead));
                            restartGameButton.setText(getString(R.string.restart));
                            enemyAttackTimer.cancel();
                        }
                        if (!match.isEnemyAlive()) {
                            updateScore("Victory");
                            enemyAttackTimer.cancel();
                        }
                    }
                });
            }
        }, 1000, 1000);

        minionsTimer = new Timer();
        minionsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.w("SchermataDiGioco", "minionstimer");
                match.getMoneyFromMiners();
                if (match.getMinionsNumber() > 0) {
                    match.minionsAttack();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hpAmountTextView.setText(""+match.getEnemyRemainingHp());
                        moneyAmountTextView.setText(match.getMoney()+ "");
                        enemyFaceImageView.setImageResource(match.getEnemyFaceDrawableId());
                        if (!match.isEnemyAlive()) {
                            gameOver();
                            gameEndTextView.setText(getString(R.string.you_won));
                            restartGameButton.setText(getString(R.string.next_level));
                            updateScore("Victory");
                            minionsTimer.cancel();
                        }
                        if (!match.isPlayerAlive()) {
                            updateScore("Loss");
                            saveScore();
                            minionsTimer.cancel();
                        }
                    }
                });
            }
        }, 1000, 2000);
    }

    /**
     * In case player dies, the score is saved in database
     */
    private void saveScore () {
        if (!alreadySaved) {
            Log.w(match.getPlayerName(), match.getScore()+"");
            ContentValues cv = new ContentValues();
            cv.put(ScoresContract.ScoresEntry.PLAYER_NAME, match.getPlayerName());
            cv.put(ScoresContract.ScoresEntry.PLAYER_SCORE, match.getScore());

            alreadySaved = true;

            getContentResolver().insert(ScoresContract.ScoresEntry.CONTENT_URI, cv);
        }
    }
}
