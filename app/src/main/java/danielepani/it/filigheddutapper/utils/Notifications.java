package danielepani.it.filigheddutapper.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import danielepani.it.filigheddutapper.R;
import danielepani.it.filigheddutapper.SchermataDiGioco;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * This is a static class with notification utilities
 */

public class Notifications {
    public static final int ID = 1178;
    public static final String CHANNEL_ID = "imthere-notification";

    public static final int GAME_INTENT_ACTION = 99;


    /**
     * Create and display the notification
     * @param context the activity that is launching it. In this case, the gaming screen
     */
    public static void imStillThere (Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.still_there_notification_name), NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .setSmallIcon(R.drawable.emoji_angry)
                .setVibrate(new long [] {0, 1000, 1000, 1000})
                .setContentTitle(context.getString(R.string.still_there_notification_name))
                .setContentText(context.getString(R.string.still_there_notification_body))
                .addAction(backToGame(context));
        manager.notify(ID, builder.build());
    }

    /**
     * Cancel the notification
     * @param context the activity that is launching it. In this case, the gaming screen
     */
    public static void iSawYouGaveMeAttention (Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    /**
     * Action to be taken when you select the Back to game action
     * @param context
     * @return Action resuming game
     */
    private static NotificationCompat.Action backToGame (Context context) {
        Intent gameIntent = new Intent(context, SchermataDiGioco.class);
        gameIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, GAME_INTENT_ACTION, gameIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Action backToGameAction = new NotificationCompat.Action(R.drawable.ic_play_arrow_black_24dp, context.getString(R.string.back_to_game_notification_action), pendingIntent);
        return backToGameAction;
    }
}
