package danielepani.it.filigheddutapper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Settings activity. Just a wrapper for a PreferenceFragment
 * @see Settaggi
 *
 */

public class SettaggiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settaggi);
    }
}
