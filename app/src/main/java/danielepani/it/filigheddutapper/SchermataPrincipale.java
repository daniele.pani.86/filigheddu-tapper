package danielepani.it.filigheddutapper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Main screen, right after app has been started
 */

public class SchermataPrincipale extends AppCompatActivity {

    //Activity's views
    private View bgSplashView;
    private TextView textView;
    private Button yesContinue;
    private Button noStartNew;

    /**
     * onCreate is like a constructor: it assigns all instance variables and handles the instanceState, if any
     *
     * @param savedInstanceState the bundle containing all match data, if activity was put in background
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_principale);

        bgSplashView = findViewById(R.id.bg_begin);
        textView = findViewById(R.id.textView);
        yesContinue = findViewById(R.id.yes_continue_button);
        noStartNew = findViewById(R.id.button);
    }

    /**
     * Game can start
     */

    private void beginPlaying () {
        Log.i("SchermataPrincipale", "Funziono");
        if (SchermataDiGioco.currentInstance != null)
        SchermataDiGioco.currentInstance.finish();

        Intent beginPlayingIntent = new Intent(this, SchermataDiGioco.class);
        startActivity(beginPlayingIntent);
    }

    /**
     * Called when you tap the smile; it starts or resumes a game
     * @param view
     */

    public void iWAnnaPlay (View view) {
        if (SchermataDiGioco.isRunning) {
            bgSplashView.setVisibility(View.VISIBLE);
            textView.setVisibility(View.VISIBLE);
            yesContinue.setVisibility(View.VISIBLE);
            noStartNew.setVisibility(View.VISIBLE);
        } else {
            beginPlaying();
        }
    }

    /**
     * Wrapper for beginPlaying(), to make it available as button action
     * @param view
     */
    public void beginPlaying (View view) {
        beginPlaying();
    }

    /**
     * Resumes a game
     *
     * @param view
     */
    public void continuePlaying (View view) {
        Log.i("SchermataPrincipale", "Funziono");
        Intent beginPlayingIntent = new Intent(this, SchermataDiGioco.class);
        beginPlayingIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(beginPlayingIntent);
    }

    /**
     * Create the options Menu
     * @param menu the activity's menu
     * @return bool
     * @link https://developer.android.com/guide/topics/ui/menus
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Handles the actions to be taken when a menu item is selected
     * @param item the selected menu item
     * @return bool true if an item has been selected, false otherwise
     * @link https://developer.android.com/guide/topics/ui/menus
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.exit:
                finish();
                moveTaskToBack(true);
                return true;
            case R.id.menu_prefs:
                Intent prefIntent = new Intent(getApplicationContext(), SettaggiActivity.class);
                startActivity(prefIntent);
                return true;

            case R.id.scores_menu:
                Intent scoresIntent = new Intent(getApplicationContext(), Punteggi.class);
                startActivity(scoresIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
