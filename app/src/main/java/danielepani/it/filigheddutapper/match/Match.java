package danielepani.it.filigheddutapper.match;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import danielepani.it.filigheddutapper.R;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * The Match object
 *
 */

public class Match {
    private int enemyInitialAttackPower;
    private int enemyInitialHp;
    private int enemyAttackPower;
    private int enemyRemainingHp;
    private int enemyFaceDrawableId;

    private String playerName;
    private int playerInitialHp;
    private int playerRemainingHp;
    private int playerSwordPower = 1;
    private int minionsPower = 1;
    private int minionsNumber = 0;
    private int minersPower = 1;
    private int minersNumber = 0;
    private int money=0;
    private int moneyPerTick=1;

    private int step = 1;
    private int score = 0;

    private final int costOne = 10;
    private final int costTwo = 20;
    private final int costThree = 25;
    private int improveEarningsCost = 5;

    public Match(String playerName, int enemyInitialHp, int playerInitialHp) {
        this.playerName = (playerName.equals("")) ? "Anonymous" : playerName;
        this.enemyInitialHp = enemyInitialHp;
        this.playerInitialHp = playerInitialHp;
        enemyAttackPower = enemyInitialHp/100;
        enemyRemainingHp = enemyInitialHp;
        playerRemainingHp = playerInitialHp;
        enemyInitialAttackPower = enemyAttackPower;
        enemyFaceDrawableId = R.drawable.emoji_smiling;
    }

    /**
     * Change the enemy's smile accordingly
     */
    private void expressEnemyMood () {
        if (enemyRemainingHp > enemyInitialHp*step/2) {
            enemyFaceDrawableId = R.drawable.emoji_smiling;
        } else {
            if (enemyRemainingHp > enemyInitialHp*step/4) {
                enemyFaceDrawableId = R.drawable.emoji_worried;
            } else {
                enemyFaceDrawableId = R.drawable.emoji_angry;
            }
        }
    }

    /**
     * Attack from the enemy
     */
    public void enemyAttack() {
        if (enemyRemainingHp>0) {
            if (playerRemainingHp >= enemyAttackPower) playerRemainingHp -= enemyAttackPower;
            else playerRemainingHp = 0;
        }
    }

    /**Attack from the player*/
    public void playerAttack () {
        if (playerRemainingHp > 0) {
            if (enemyRemainingHp >= playerSwordPower)
                enemyRemainingHp -= playerSwordPower;
            else enemyRemainingHp = 0;
        }
        expressEnemyMood();
    }

    /*Attack from minions*/
    public void minionsAttack () {
        if (playerRemainingHp > 0) {
            if (enemyRemainingHp >= minionsPower*minionsNumber)
                enemyRemainingHp -= minionsPower*minionsNumber;
            else enemyRemainingHp = 0;
        }
        expressEnemyMood();
    }

    /*Reset player data to play again*/
    public void reinitializePlayer () {
        if (playerRemainingHp == 0) {
            money = 0;
            playerSwordPower = 1;
            minionsNumber = 0;
            minionsPower = 1;
            minersNumber = 0;
            minersPower = 1;
            moneyPerTick = 1;
            improveEarningsCost = 5;
            score = 0;
        }
        playerRemainingHp = playerInitialHp;
    }

    /*Next step or level*/
    public void nextStep () {
        enemyFaceDrawableId = R.drawable.emoji_smiling;
        if (enemyRemainingHp == 0) {
            step++;
            enemyRemainingHp = enemyInitialHp*step;
            enemyAttackPower = enemyInitialAttackPower*step;
            playerRemainingHp += playerInitialHp*step/10;
        } else {
            enemyRemainingHp = enemyInitialHp;
            enemyAttackPower = enemyInitialAttackPower;
            step=1;
        }
    }

    /**
     * Check if enemy is still alive
     * @return boolean true if it is
     */
    public boolean isEnemyAlive () {
        return enemyRemainingHp > 0;
    }

    /**
     * Check if player is still alive
     * @return boolean true if it is
     */
    public boolean isPlayerAlive () {
        return playerRemainingHp > 0;
    }

    /*Increase money amount*/
    public void workHard () {
        money += moneyPerTick;
    }

    /**
     * Power up player's attack
     * @param context the activity that's launching this
     */
    public void powerSwordUp (Context context) {
        if (money >= costThree) {
            playerSwordPower++;
            money -= costThree;
        } else {
            Toast.makeText(context, "Non hai abbastanza soldi. Lavora un po'!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Get one minion
     * @param context the activity that's launching this
     */
    public void getOneMinion (Context context) {
        if (money >= (costOne*(minionsNumber+1))/2) {
            money -= ((costOne*(minionsNumber+1))/2);
            minionsNumber++;
        } else {
            Toast.makeText(context, "Non hai abbastanza soldi. Lavora un po'!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Power up minions
     * @param context the activity that's launching this
     */

    public void increaseMinionPower (Context context) {
        if (money >= costTwo) {
            minionsPower++;
            money -= costTwo;
        } else {
            Toast.makeText(context, "Non hai abbastanza soldi. Lavora un po'!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Get a single miner
     * @param context the activity that's launching this
     */
    public void getOneMiner (Context context) {
        if (money >= costOne) {
            minersNumber++;
            money -= costOne;
        } else {
            Toast.makeText(context, "Non hai abbastanza soldi. Lavora un po'!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Increase the miner's capacity to produce money
     * @param context the activity that's launching this
     */
    public void increaseMinersPower (Context context) {
        if (money >= costOne/2) {
            minersPower++;
            money -= costOne/2;
        } else {
            Toast.makeText(context, "Non hai abbastanza soldi. Lavora un po'!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Get money from miners
     */
    public void getMoneyFromMiners () {
        money += minersPower*minersNumber;
    }

    /**
     * Increase earnings while working hard
     * @param context the activity that's launching this
     */
    public void improveEarnings (Context context) {
        if (money >= improveEarningsCost) {
            moneyPerTick++;
            money -= improveEarningsCost;
            improveEarningsCost+=2;
        } else {
            Toast.makeText(context, "Non hai abbastanza soldi. Lavora un po'!", Toast.LENGTH_LONG).show();
        }
    }

    /*Getters & setters*/

    public int getEnemyInitialAttackPower() {
        return enemyInitialAttackPower;
    }

    public void setEnemyInitialAttackPower(int enemyInitialAttackPower) {
        this.enemyInitialAttackPower = enemyInitialAttackPower;
    }

    /*Getters & setters */

    public String getPlayerName() {
        return playerName;
    }

    public int getEnemyInitialHp() {
        return enemyInitialHp;
    }

    public void setEnemyInitialHp(int enemyInitialHp) {
        this.enemyInitialHp = enemyInitialHp;
    }

    public int getEnemyAttackPower() {
        return enemyAttackPower;
    }

    public void setEnemyAttackPower(int enemyAttackPower) {
        this.enemyAttackPower = enemyAttackPower;
    }

    public int getEnemyRemainingHp() {
        return enemyRemainingHp;
    }

    public void setEnemyRemainingHp(int enemyRemainingHp) {
        this.enemyRemainingHp = enemyRemainingHp;
    }

    public int getPlayerInitialHp() {
        return playerInitialHp;
    }

    public void setPlayerInitialHp(int playerInitialHp) {
        this.playerInitialHp = playerInitialHp;
    }

    public int getPlayerRemainingHp() {
        return playerRemainingHp;
    }

    public void setPlayerRemainingHp(int playerRemainingHp) {
        this.playerRemainingHp = playerRemainingHp;
    }

    public int getPlayerSwordPower() {
        return playerSwordPower;
    }

    public void setPlayerSwordPower(int playerSwordPower) {
        this.playerSwordPower = playerSwordPower;
    }

    public int getMinionsPower() {
        return minionsPower;
    }

    public void setMinionsPower(int minionsPower) {
        this.minionsPower = minionsPower;
    }

    public int getMinionsNumber() {
        return minionsNumber;
    }

    public void setMinionsNumber(int minionsNumber) {
        this.minionsNumber = minionsNumber;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getEnemyFaceDrawableId() {
        return enemyFaceDrawableId;
    }

    public int getMoney() {
        return money;
    }

    public int getMinersNumber() {
        return minersNumber;
    }

    public int getMinersPower() {
        return minersPower;
    }

    public int getMoneyPerTick() {
        return moneyPerTick;
    }

    public void setEnemyFaceDrawableId(int enemyFaceDrawableId) {
        this.enemyFaceDrawableId = enemyFaceDrawableId;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setMinersPower(int minersPower) {
        this.minersPower = minersPower;
    }

    public void setMinersNumber(int minersNumber) {
        this.minersNumber = minersNumber;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setMoneyPerTick(int moneyPerTick) {
        this.moneyPerTick = moneyPerTick;
    }

    public void setImproveEarningsCost(int improveEarningsCost) {
        this.improveEarningsCost = improveEarningsCost;
    }
}
