package danielepani.it.filigheddutapper;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import danielepani.it.filigheddutapper.scoresdata.ScoresContract;
import danielepani.it.filigheddutapper.scoresdata.ScoresDbHelper;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Scores screen. Implements LoaderCallbacks to deal with a ContentProvider. Not going to change it: this is just a toy
 * TODO: display only 20 highest scores overall
 * TODO: option to delete all the scores
 *
 */

public class Punteggi extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {


    private RecyclerView scoresView;
    private ScoresAdapter mAdapter;

    /**
     * onCreate is like a constructor: it assigns all instance variables and handles the instanceState, if any
     *
     * @param savedInstanceState the bundle containing all match data, if activity was put in background
     * @link https://developer.android.com/guide/components/activities/activity-lifecycle
     *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punteggi);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        scoresView = findViewById(R.id.scores_view);

        mAdapter = new ScoresAdapter(this);

        scoresView.setLayoutManager(new LinearLayoutManager(this));
        scoresView.setAdapter(mAdapter);

        getLoaderManager().initLoader(0, null, this);
    }

    /**
     * Create the options Menu
     * @param menu the activity's menu
     * @return bool
     * @link https://developer.android.com/guide/topics/ui/menus
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Handles the actions to be taken when a menu item is selected
     * @param item the selected menu item
     * @return bool true if an item has been selected, false otherwise
     * @link https://developer.android.com/guide/topics/ui/menus
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, SchermataPrincipale.class);
                startActivity(homeIntent);
                return true;
            case R.id.exit:
                finish();
                moveTaskToBack(true);
                return true;
            case R.id.menu_prefs:
                Intent prefIntent = new Intent(getApplicationContext(), SettaggiActivity.class);
                startActivity(prefIntent);
                return true;

            case R.id.scores_menu:
                Intent scoresIntent = new Intent(getApplicationContext(), Punteggi.class);
                startActivity(scoresIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Create a loader for content provider. It retrieves scores from local db
     * @param id loader id
     * @param args
     * @return Loader<Cursor> where Cursor contains the db query results
     * @see danielepani.it.filigheddutapper.scoresdata.ScoresContentProvider
     * @see android.app.LoaderManager.LoaderCallbacks
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<Cursor>(this) {
            Cursor cursor = null;

            @Override
            protected void onStartLoading() {
                if (cursor != null) {
                    deliverResult(cursor);
                } else {
                    forceLoad();
                }
            }

            @Override
            public Cursor loadInBackground() {
                //ContentResolver calls our ScoresContentProvider
                try {
                    return getContentResolver().query(ScoresContract.ScoresEntry.CONTENT_URI,
                            null,
                            null,
                            null,
                            ScoresContract.ScoresEntry.PLAYER_GAME_DATE + " DESC");
                } catch (Exception e) {
                    Log.e("LoadInBackground", "An error has occurred", e);
                    return null;
                }
            }
        };
    }

    /**
     * Here data are set in ScoresAdapter
     * @param loader returned by onCreateLoader
     * @param data cursor containing query values
     * @see ScoresAdapter
     * @see android.app.LoaderManager.LoaderCallbacks
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    /**
     * This function is required to implement LoaderCallbacks
     * @param loader
     * @see android.app.LoaderManager.LoaderCallbacks
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
