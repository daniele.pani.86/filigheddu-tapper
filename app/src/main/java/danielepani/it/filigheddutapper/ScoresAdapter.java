package danielepani.it.filigheddutapper;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import danielepani.it.filigheddutapper.scoresdata.ScoresContract;

/**
 * @author Daniele Pani (sharkattack86)
 * @version 1.0.0
 * @since 1.0.0
 *
 * ScoresAdapter. Once values are retrieved from local db, they're displayed thanks to this class
 */

public class ScoresAdapter extends RecyclerView.Adapter<ScoresAdapter.ScoresHolder> {
    private Context mContext;
    private Cursor mCursor;

    public ScoresAdapter(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * ViewHolder is defined at the end of this class itself. It contains the views in a score row
     * @param holder the ViewHolder
     * @param position cursor's position
     */
    @Override
    public void onBindViewHolder(@NonNull ScoresHolder holder, int position) {
        if (!mCursor.moveToPosition(position)) return;

        String playerName = mCursor.getString(mCursor.getColumnIndex(ScoresContract.ScoresEntry.PLAYER_NAME));
        int playerScore = mCursor.getInt(mCursor.getColumnIndex(ScoresContract.ScoresEntry.PLAYER_SCORE));
        String gameDate = mCursor.getString(mCursor.getColumnIndex(ScoresContract.ScoresEntry.PLAYER_GAME_DATE));
        int id = mCursor.getInt(mCursor.getColumnIndex(ScoresContract.ScoresEntry._ID));

        holder.positionTextView.setText((1+position)+"");
        holder.playerNameTextView.setText(playerName);
        holder.playerScoreTextView.setText(playerScore + "");
        holder.playerGameDateTextView.setText(gameDate);
        holder.itemView.setTag(id);
    }

    /**
     * This method is custom. It acts as a setter, or changes cursor if we need live update (not in this case)
     * @param newCursor a cursor in this case
     */
    public void swapCursor(Cursor newCursor) {
        if (mCursor != null) mCursor.close();
        mCursor = newCursor;
        if (newCursor != null) {
            // Force the RecyclerView to refresh
            this.notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ScoresHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view = inflater.inflate(R.layout.score_single_item, parent, false);

        return new ScoresHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    /**
     * This is actually the ViewHolder we were talking about
     */
    class ScoresHolder extends RecyclerView.ViewHolder {
        TextView positionTextView;
        TextView playerNameTextView;
        TextView playerScoreTextView;
        TextView playerGameDateTextView;
        public ScoresHolder(View itemView) {
            super(itemView);
            positionTextView = itemView.findViewById(R.id.position);
            playerNameTextView = itemView.findViewById(R.id.player_name);
            playerScoreTextView = itemView.findViewById(R.id.player_score);
            playerGameDateTextView = itemView.findViewById(R.id.game_date);
        }
    }
}
